
//devctrl-proto-package
import {AP400Communicator} from "./ClearOne/AP400Communicator";
import {XAP800Communicator} from "./ClearOne/XAP800Communicator";

module.exports = {
    communicators : {
        'ClearOne/AP400' : AP400Communicator,
        'ClearOne/XAP800' : XAP800Communicator
    }
};