import {
    TCPCommunicator,
    TCPCommand,
    EndpointCommunicator,
    IEndpointCommunicatorConfig
} from "@devctrl/lib-communicator";
import { commands } from "./AP400Controls";
import {IClearOneCommandConfig, ClearOneCommand} from "./ClearOneCommand";



export interface IAP400CommandConfig {
    cmdStr: string;
    ioList?: any;
    ctor: typeof ClearOneCommand;
    control_type: string;
    usertype: string;
    readonly?: boolean;
    updateTerminator?: string;
    templateConfig?: any;
}

export class AP400Communicator extends TCPCommunicator {
    device = "#30";

    constructor(config: IEndpointCommunicatorConfig) {
        super(config);
    }

    connect() {
        this.log("connecting to AP 400", EndpointCommunicator.LOG_CONNECTION);
        //super.connect();
    }


    preprocessLine(line: string) : string {
        // Strip a leading prompt
        let start = "AP 400> ";
        if (line.substring(0, start.length) == start) {
            return line.slice(start.length);
        }

        return line;
    }



    buildCommandList() {
        // First build a command list
        for (let cmdIdx in commands) {
            let cmdDef = commands[cmdIdx];
            let cmdConfig : IClearOneCommandConfig = {
                endpoint_id: this.config.endpoint._id,
                cmdStr: cmdDef.cmdStr,
                control_type: cmdDef.control_type,
                usertype: cmdDef.usertype,
                channel: '',
                channelName: '',
                device: this.device,
                templateConfig: cmdDef.templateConfig || {}
            };

            if (cmdDef.updateTerminator) {
                cmdConfig.updateTerminator = cmdDef.updateTerminator;
            }

            if (cmdDef.ioList) {
                for (let ioStr in cmdDef.ioList) {
                    cmdConfig.channel = ioStr;
                    cmdConfig.channelName = cmdDef.ioList[ioStr];

                    let cmd = new cmdDef.ctor(cmdConfig);
                    this.commands[cmd.cmdStr] = cmd;
                }
            }
            else {
                let cmd : TCPCommand = new cmdDef.ctor(cmdConfig);
                this.commands[cmd.cmdStr] = cmd;
            }
        }
    }
}